import org.testng.annotations.Test;

import io.restassured.RestAssured;
import io.restassured.http.Method;
import io.restassured.response.Response;
import io.restassured.specification.RequestSpecification;

public class SimpleGETTest {

	
	@Test
	public void getWeatherDetail() {
		
		RestAssured.baseURI = "https://bookstore.toolsqa.com";
		
		RequestSpecification httprequest = RestAssured.given();
		
		Response response = httprequest.request(Method.GET, "/BookStore/v1/Books");
		
		String responseBody = response.getBody().asString();
		
		System.out.println("Response Body is =>  " + responseBody);
	}
	
}
