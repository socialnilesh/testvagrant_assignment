# TestVagrant_Assignment

To run this project follow below steps.

<<-- Prerequisite NDTV weather web URL : https://social.ndtv.com/static/Weather/report/?pfrom=home-ndtv_topsubnavigation should not be down -->>

1. Import the project as general project
2. Go to configure and convert it to Gradle STS project
3. Refresh the Gradle to update the dependencies  
4. Open the RunTests.java located in src/test/java/runner and run as Junit Test
5. It will run all three scenario and generate the cucumber report

