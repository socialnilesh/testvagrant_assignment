$(document).ready(function() {var formatter = new CucumberHTML.DOMFormatter($('.cucumber-report'));formatter.uri("file:src/test/resources/features/NDTVweatherValidation.feature");
formatter.feature({
  "name": "NDTV Weather validation",
  "description": "",
  "keyword": "Feature",
  "tags": [
    {
      "name": "@NDTV"
    }
  ]
});
formatter.scenarioOutline({
  "name": "Verifty that weather information is available on UI for a vaild city",
  "description": "",
  "keyword": "Scenario Outline",
  "tags": [
    {
      "name": "@weather_pune"
    }
  ]
});
formatter.step({
  "name": "user land on the page URL \u0027\u003curl\u003e\u0027",
  "keyword": "Given "
});
formatter.step({
  "name": "select the weather tab on home page",
  "keyword": "When "
});
formatter.step({
  "name": "select the city \u0027\u003ccity\u003e\u0027 to get weather details",
  "keyword": "And "
});
formatter.step({
  "name": "verify that selected city \u0027\u003ccity\u003e\u0027 is available on map",
  "keyword": "Then "
});
formatter.step({
  "name": "verify that weather info of the city \u0027\u003ccity\u003e\u0027 is available on map",
  "keyword": "Then "
});
formatter.examples({
  "name": "",
  "description": "",
  "keyword": "Examples",
  "rows": [
    {
      "cells": [
        "url",
        "city"
      ]
    },
    {
      "cells": [
        "NDTV_url",
        "Pune"
      ]
    }
  ]
});
formatter.scenario({
  "name": "Verifty that weather information is available on UI for a vaild city",
  "description": "",
  "keyword": "Scenario Outline",
  "tags": [
    {
      "name": "@NDTV"
    },
    {
      "name": "@weather_pune"
    }
  ]
});
formatter.step({
  "name": "user land on the page URL \u0027NDTV_url\u0027",
  "keyword": "Given "
});
formatter.match({
  "location": "stepDefinitions.NDTVweatherValidation_Definition.user_land_on_the_page_URL(java.lang.String)"
});
