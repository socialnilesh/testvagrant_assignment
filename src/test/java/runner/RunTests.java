package runner;

import org.junit.runner.RunWith;

import io.cucumber.junit.Cucumber;
import io.cucumber.junit.CucumberOptions;

@RunWith(Cucumber.class)

@CucumberOptions(
					plugin = {"pretty", "html:target/cucumber-html-report",
							  "json:target/cucumber.json",
							  "junit:target/cucumber_junit/cucumber.xml"},
					tags = {"@weather_pune"},
					features= {"src/test/resources"},
					glue = {"stepDefinitions"},
					monochrome = true
		)

public class RunTests {

}
