package stepDefinitions;


import io.cucumber.datatable.DataTable;
import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;

import utility.BaseConfig;
import utility.DataTableReader;
import utility.RequestHandler;

public class APIDefinition extends BaseConfig {
	
	RequestHandler req = new RequestHandler();
	
	public static String cityTempAPI;
	
	@Given("user wants to perform operation for URI {string}")
	public void user_wants_to_perform_operation_for_URI(String baseURI) {
		
	    req.setBaseURI(BaseConfig.getProperty(baseURI));
	}

	@When("user set the header as below")
	public void user_set_the_header_as_below(DataTable dataTable) {
		//System.out.println(dataTable.asMap(String.class, String.class));
		req.setHeaders(DataTableReader.getHeaderDataTable(dataTable));
	}
	
	@When("user set required parameters as below")
	public void user_set_required_parameters_as_below(DataTable dataTable) {
		
		req.setRequestParameters(dataTable);
	}

	
	@When("user want to perform {string} for {string} with {string} below parameters")
	public void user_want_to_perform_for_with_below_parameters(String operation, String service, String bodyFilePath, DataTable dataTable) {
		
		req.sendRequestWithParameter(operation, service, bodyFilePath, dataTable);
		
	}

	@Then("webservice should respond responce code {string}")
	public void webservice_should_respond_responce_code(String expectedCode) {
	   req.assertResponseCode(Integer.parseInt(expectedCode));
	}

	@Then("webservice should send response as following")
	public void webservice_send_response_as_following(DataTable dataTable) {
	    req.verifyJsonResponse(dataTable);
	}
	
	@Then("get the required value {string} from response")
	public void get_the_required_value_from_response(String value) {
		
		cityTempAPI = req.getReqValue(value);
		
	}



}
