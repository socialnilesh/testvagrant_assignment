package stepDefinitions;

import org.openqa.selenium.WebDriver;

import io.cucumber.java.After;
import io.cucumber.java.Before;
import io.cucumber.java.en.And;
import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import utility.BaseConfig;
import utility.BaseTest;

public class NDTVweatherValidation_Definition  {
	
		
	BaseTest test = new BaseTest();
	
	@Given("user land on the page URL {string}")
	public void user_land_on_the_page_URL(String url) throws Throwable {
		test.setBrowser();
		test.launchURL(BaseConfig.getProperty(url));
		
	}

	@When("select the weather tab on home page")
	public void select_the_weather_tab_on_home_page() throws Throwable {
		test.landOnWeatherPage();
	    
	}

	@And("select the city {string} to get weather details")
	public void select_the_city_Pune_to_get_weather_details(String city) throws Throwable {
		test.selectCityOnWeatherPage(city);
	    
	}

	@Then("verify that selected city {string} is available on map")
	public void verify_that_selected_city_is_available_on_map(String city) throws Throwable {
		test.verifyCityOnMap(city);
	    
	}

	@Then("verify that weather info of the city {string} is available on map")
	public void verify_that_weather_info_of_the_city_is_available_on_map(String city) throws Throwable {
		test.verifyWeatherInfo(city);
		
	}
	
	@Given("validate the temp differences of UI and API")
	public void validate_the_temp_differences_of_UI_and_API() throws Throwable {
		test.compareTempAPIandUI();
		
	}
	
	
	@After public void afterScenario(){
		test.closeDriver();
    }
	
//	@Before public void beforeScennario(){
//		test.setBrowser();
//	    }

}
