package utility;


import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.TimeUnit;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import pages.NDTVPage;
import stepDefinitions.APIDefinition;

public class BaseTest {
	
	
	WebDriver driver;
	
    NDTVPage ndtvp;
    
    String tempUI;
    
	        
	public void setBrowser(){

    	if(BaseConfig.getProperty("Browser").equalsIgnoreCase("chrome")) {
    	Map<String, Object> prefs = new HashMap<String, Object>();

		prefs.put("profile.default_content_setting_values.notifications", 1);

		ChromeOptions options = new ChromeOptions();
		options.addArguments("--disable-notifications");
		
		System.setProperty("webdriver.chrome.driver", "./driver/chromedriver.exe");
		 driver = new ChromeDriver(options);
		 driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
		 driver.manage().window().maximize();
        
		}

    }
	
	public void launchURL(String url) {
		driver.get(url);
		
	}
	
	public void test_Home_Page_Appear_Correct(){
		ndtvp = new NDTVPage(driver);
		ndtvp.clickexpand();
     }
	
	public void landOnWeatherPage() {
		ndtvp = new NDTVPage(driver);
		ndtvp.navigateWeatherPage();
	}
	
	public void closeDriver() {
		try {
		driver.close();
		}catch(NullPointerException e ){
			e.printStackTrace();
		}
	}
	
	public void selectCityOnWeatherPage(String city) {
		ndtvp = new NDTVPage(driver);
		ndtvp.searchCity(city);
	}
	
	public void verifyCityOnMap(String city) {
		ndtvp = new NDTVPage(driver);
		ndtvp.checkCityOnMap(city);
	}
	
	public void verifyWeatherInfo(String city) {
		ndtvp = new NDTVPage(driver);
		ndtvp.weatherInfoOfCity(city);
	}
	
	public void compareTempAPIandUI() {
		float tempAPI = Float.parseFloat(APIDefinition.cityTempAPI);
		float tempUI = Float.parseFloat(NDTVPage.cityTempUI) + 273;
//		System.out.println(tempAPI);
//		System.out.println(tempUI);
		int chk = ndtvp.tempComparator(tempAPI, tempUI);
		switch (chk) {
        case 1:
            System.out.println("Temp difference is more than 2 i.e. out of range");
            break;
        case 2:
        	System.out.println("Temp difference is not more than 2 i.e. in range");
            break;
        case 3:
        	System.out.println("Temp difference is not evaluated");
            break;
		}
	}
}
