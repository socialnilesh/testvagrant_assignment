package utility;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.Map;
import org.apache.commons.io.IOUtils;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import io.cucumber.datatable.DataTable;
import com.jayway.jsonpath.Configuration;
import com.jayway.jsonpath.spi.json.JacksonJsonNodeJsonProvider;
import com.jayway.jsonpath.spi.mapper.JacksonMappingProvider;

import com.jayway.jsonpath.JsonPath;


public class JsonModifier {
	
	//public final String requestPath = "./src/test/resources/apiRequestData/users_api/user_api1.json";
	
	public JsonModifier() {}
	
	public static final Configuration configuration = Configuration.builder()
			.jsonProvider(new JacksonJsonNodeJsonProvider())
			.mappingProvider(new JacksonMappingProvider()).build();
	
	public static void modifyJson(String filePath, DataTable dataTable) {
		    ObjectMapper mapper = new ObjectMapper();
		    Map<String, Object> addmap = DataTableReader.getDataFromData(dataTable, "xpath", "value");
	        
	       // List<String> addlist = new ArrayList<String>();
	       
	        //addmap.put("listkey", dataTable);
	 
	        /**
	         * Convert Map to JSON and write to a file
	         */
		   
		    if(addmap.size()!=0) {
	        try {
	            mapper.writeValue(new File(filePath), addmap);
	        } catch (Exception e) {
	            e.printStackTrace();
	        }
		    }
	 
	    }
	
	
	public static Map<String, Object> getMapFromJson(String filePath){
		
		ObjectMapper mapper = new ObjectMapper();
		Map<String, Object> rmap=null;
				
        /**
         * Read JSON from a file into a Map
         */
        try {
        	rmap = mapper.readValue(new File(
            		filePath), new TypeReference<Map<String, Object>>() {
            });
        	System.out.println(rmap);
        } catch (Exception e) {
            e.printStackTrace();
        }
           
       return rmap;
	}
	
	public static String parseJsonToValue(String filePath, String nodePath) {
		String jsonValue = null;
		FileReader reader = null;
		try {
			reader = new FileReader(filePath);
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		String expectedJson = null;
		try {
			expectedJson = IOUtils.toString(reader);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
//		JsonNode updatedJson = JsonPath.using(configuration).parse(expectedJson).read(nodePath);
		jsonValue =	JsonPath.using(configuration).parse(expectedJson).read(nodePath).toString();
//		System.out.println(s);
//		if(updatedJson.isArray()) {
//			jsonValue = updatedJson.get(0).asText();
//			
//		} else {
//			jsonValue = updatedJson.asText();
//		}
		
		return jsonValue;
		
		}
	
	
	public static String setJsonNodeInRequest(String filePath, String nodePath, String setValue)  {
		String jsonString = null;
		FileReader reader = null;
		try {
			reader = new FileReader(filePath);
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		String expectedJson = null;
		try {
			expectedJson = IOUtils.toString(reader);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		jsonString = JsonPath.using(configuration).parse(expectedJson).set(nodePath, setValue).jsonString();
		//System.out.println(jsonString);
		
		return jsonString;
		
	}
	
	public static String setJsonNodeInRequest(String filePath, Map<String, Object> nodeMap) {
		String jsonString = null;
		FileReader reader = null;
		try {
			reader = new FileReader(filePath);
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		String expectedJson = null;
		try {
			expectedJson = IOUtils.toString(reader);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	//	System.out.println(nodeMap);
		
		for(String key : nodeMap.keySet()) {
			if(nodeMap.get(key)!=null){
			expectedJson = JsonPath.using(configuration).parse(expectedJson).set(key, nodeMap.get(key)).jsonString();
			}
		}
		jsonString = expectedJson;
		System.out.println(jsonString);
		
		return jsonString;
		
	}

}
