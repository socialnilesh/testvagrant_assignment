package utility;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import io.cucumber.datatable.DataTable;

public class DataTableReader {

	public DataTableReader() {}
	
		
	public static Map<String, String> getDataFromDataTable(DataTable dataTable) {
		List<Map<String, String>> table = dataTable.asMaps(String.class, String.class);
		Map<String, String> rmap = new HashMap<>();
		for(Map<String, String> m : table) {
			rmap.put(m.get("key"), m.get("value"));
		}
		//System.out.println(rmap);
		return rmap;
	}
	
	public static Map<String, String> getDataFromDataTable(DataTable dataTable, String key, String value) {
		List<Map<String, String>> table = dataTable.asMaps(String.class, String.class);
		Map<String, String> rmap = new HashMap<>();
		for(Map<String, String> m : table) {
			if(m.get(key)!=null) {
			rmap.put(m.get(key), m.get(value));
			}
		}
		//System.out.println(rmap);
		return rmap;
	}
	
	public static Map<String, Object> getDataFromData(DataTable dataTable, String key, String value) {
		List<Map<String, Object>> table = dataTable.asMaps(String.class, String.class);
		//dataTable.asMap(String.class, String.class)
		Map<String, Object> rmap = new HashMap<>();
		for(Map<String, Object> m : table) {
			if(m.get(key)!=null) {
			//rmap.put(m.get(key), m.get(value));
				rmap.put((String) m.get(key), m.get(value));
			}
		}
		//System.out.println(rmap);
		return rmap;
	}
	
	public static Map<String, String> getHeaderDataTable(DataTable dataTable) {
		
		return getDataFromDataTable(dataTable, "headerkey", "headervalue");
	}
	
    public static Map<String, String> getPathParamDataTable(DataTable dataTable) {
		
		return getDataFromDataTable(dataTable, "pathParamName", "pathParamValue");
	}
	
    public static Map<String, String> getQueryParamDataTable(DataTable dataTable) {
		
		return getDataFromDataTable(dataTable, "queryParamName", "queryParamValue");
	}
    
    
	
}
