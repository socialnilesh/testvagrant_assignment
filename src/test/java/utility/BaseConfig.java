package utility;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.time.Duration;
import java.util.List;
import java.util.Properties;
import org.apache.commons.io.IOUtils;
import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.FluentWait;
import com.google.common.base.Function;

public class BaseConfig {
	
	private static final String TEST_DEFAULTS = "UserTestDefault.properties";
	
	private static Properties defaultProperties = new Properties();
	
	final static Logger logger = LogManager.getLogger(BaseConfig.class);

	
	static {
		BufferedReader reader;
		try {
			reader = new BufferedReader(new FileReader(TEST_DEFAULTS));
			
			try {
				defaultProperties.load(reader);
				reader.close();
			}	catch (IOException e) {
				e.printStackTrace();
				logger.debug("Exception occurs while loading the property file " + TEST_DEFAULTS );
				
			}
		} catch (FileNotFoundException e) {
			e.printStackTrace();
			logger.debug("File '{}' not found for " + TEST_DEFAULTS, e);
			throw new RuntimeException("Configuration.properties not found at " + TEST_DEFAULTS);
			
		} 	
	}
	
	public BaseConfig() {
		
	}
	
	
	
		
	public void clickUsingJS(WebDriver driver, WebElement element) {
		try {
			element.isDisplayed();
			JavascriptExecutor js = (JavascriptExecutor)driver;
			js.executeScript("arguments[0].click();", element);
		} catch(Exception e) {
			e.printStackTrace();
		} catch(Error er) {
			er.printStackTrace();
		}
	}
	
	public void waitUsingFluentWait(WebDriver driver, final WebElement element, long waitSecond, long pollingMiliSec) {
		FluentWait<WebDriver> wait = new FluentWait<WebDriver>(driver)
				.withTimeout(Duration.ofSeconds(waitSecond))
				.pollingEvery(Duration.ofMillis(pollingMiliSec))
				.ignoring(NoSuchElementException.class);
		
		Function<WebDriver, Boolean> function = new Function<WebDriver, Boolean>(){

			public Boolean apply(WebDriver driver) {
				if(element.isDisplayed()) {
					return true;
				} else {
					return false;
				}
			}
			
		};
		wait.until(function);
	}
	
	
	
	public static String getProperty(String propertyName) {
		String propertyValue = null;
		if(defaultProperties.getProperty(propertyName) != null) {
			propertyValue = defaultProperties.getProperty(propertyName);
		}
//		else if(StringUtils.isEmpty(propertyValue)) {
//			propertyValue = defaultProperties.getProperty(propertyName);
//		}
		else {
			throw new RuntimeException("property name or value are not specified in the Configuration.properties file.");		
		}
	
		return propertyValue;
	}
	
	public String readFile(String fileName) {
		FileReader fileReader = null;
		String fileOutput="";
		try {
			fileReader = new FileReader(fileName);
			
			fileOutput = IOUtils.toString(fileReader);
			
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}finally{
//		fileReader.close();
		IOUtils.closeQuietly(fileReader);
		}
		return fileOutput;
	}
	
	public static boolean compareTwoList(List ls1, List ls2)  
	{  
	//converts List into String and checks string are equal or not  
	return ls1.toString().contentEquals(ls2.toString())?true:false;  
	}

}
