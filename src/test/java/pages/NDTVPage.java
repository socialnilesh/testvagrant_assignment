package pages;


import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.NoSuchElementException;
import org.junit.Assert;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

import utility.BaseConfig;


public class NDTVPage extends BaseConfig{
	
	WebDriver driver;
	 
	    @FindBy(xpath = "//div[@class='seclevelnav']")
	    WebElement secLevelNavDiv;

	    @FindBy(id = "h_sub_menu")	
	    WebElement expand_menu;
	    
	    @FindBy(xpath = "//a[contains(text(),'WEATHER')]")
	    WebElement weatherLink;
	    	    
	    @FindBy(xpath = "//input[@id='searchBox']")
	    private WebElement citySearchBox;
	    
	    @FindBy(xpath = "//div[@class='leaflet-popup-content']/div/div/span[2]")
	    private WebElement cityPopUp;
	    
	    Map<String, String> weatherInfoUI;
	    List<String> expectedWeatherPram;
	    public static String cityTempUI;

	    public NDTVPage(WebDriver driver){
	    	
	    	this.driver = driver;

	        PageFactory.initElements(driver, this);

	    }
    
	   	    
	    public void clickexpand() {
	    	expand_menu.click();
	    	
	    }
	    
	    public void navigateWeatherPage() {
	    	waitUsingFluentWait(driver, expand_menu, 10, 50);
	    	expand_menu.click();
	    	weatherLink.click();
	    }
	    
	    public void searchCity(String city) {
	    	waitUsingFluentWait(driver, citySearchBox, 5, 100);
	    	citySearchBox.sendKeys(city);
	    	WebElement selectCity = this.driver.findElement(By.xpath("//input[@id='"+city+"' and @type='checkbox']"));
	    	try{
	    		if(selectCity.isDisplayed()) {
	    			selectCity.click();
	    		}else {
	    		
	    			Assert.assertTrue("Entered City weather is not available", false);
	    		}
	    	} catch(NoSuchElementException ex) {
	    		ex.printStackTrace();
	    	}
	    	    	   	
	    }
	    
	    public void checkCityOnMap(String city) {
	    	try{
	    		WebElement cityOnMap = this.driver.findElement(By.xpath("//div[@class='cityText' and text()='"+city+"']"));
	    		if(cityOnMap.isDisplayed()) {
	    			System.out.println("Selected City is available on Map");
	    		}else {
	    		//	System.out.println("Entered City weather is not available");
	    			Assert.assertTrue("Selected City is not available on Map", false);
	    		}
	    	} catch(NoSuchElementException ex) {
	    		ex.printStackTrace();
	    	}
	    }
	    
	    public void weatherInfoOfCity(String city) {
	    	weatherInfoUI = new HashMap<String, String>();
	    	expectedWeatherPram =new ArrayList<String>(Arrays.asList("Condition", "Wind, Humidity", "Temp in Degrees", "Temp in Fahrenheit"));
	    	WebElement cityOnMap = this.driver.findElement(By.xpath("//div[@class='cityText' and text()='"+city+"']"));
	    	cityOnMap.click();
	    	if(cityPopUp.getText().contains(city)) {
	    		List<WebElement> weather = this.driver.findElements(By.xpath("//div[@class='leaflet-popup-content']/div/span"));
	    		for(WebElement ele: weather) {
	    			WebElement weatherInfo = ele.findElement(By.xpath("./b"));
	    			String [] info = weatherInfo.getText().split(":", 2);
	    		    weatherInfoUI.put(info[0].trim(), info[1].trim());
	    			}
	    		//System.out.println(weatherInfoUI);
	    		cityTempUI = weatherInfoUI.get("Temp in Degrees");
	    		List<String> actualWeatherParam = new ArrayList<String>(weatherInfoUI.keySet());
	    		
	    		Assert.assertTrue("Required weathe info is not available", BaseConfig.compareTwoList(expectedWeatherPram, actualWeatherParam));
	    	}
	    	    	  	
	    } 
	    
	    public int tempComparator(float temp1, float temp2) {
	    	float absTemp = Math.abs(temp1 - temp2);
	    	//System.out.println(absTemp);
    		if(absTemp>=2) {
    			return 1;
    		}else if(absTemp<=2) {
    			return 2;
    		}else {
    			return 3;
    		}
			   		
    	}
	    

}
