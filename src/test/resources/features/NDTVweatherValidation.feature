#Author: Nilesh Singh
@NDTV
Feature: NDTV Weather validation
 
  @weather_pune
  Scenario Outline: Verifty that weather information is available on UI for a vaild city
    Given user land on the page URL '<url>'
    When select the weather tab on home page
    And select the city '<city>' to get weather details
    Then verify that selected city '<city>' is available on map 
    Then verify that weather info of the city '<city>' is available on map 

    Examples: 
      | url      | city  |
      | NDTV_url | Pune  | 
  
  @weather_pune
 Scenario Outline: validation get request for weather info of a valid city
	Given user wants to perform operation for URI "<uri>"
	When user set the header as below
	|headerkey   |headervalue     |
	|Content-Type|application/json|
	When user set required parameters as below
	|pathParamName|pathParamValue |queryParamName|queryParamValue| #Need to pass empty data if you don't have required to pass any parameter
	| id     | 2.5    |q            | Pune             |
	|        |        |appid        | 7fe67bf08c80ded756e598d6f8fedaea |   
	When user want to perform "<operation>" for "<service>" with "<bodyFilePath>" below parameters
	| |     # Need pass empty data table if you don't required to pass body table
	
	Then webservice should respond responce code "<code>"
	Then webservice should send response as following
	|xpath           |value  |
	|id          |1259229 |
	|name          |Pune |
	Then get the required value "<temp>" from response
	
	
	Examples: 
	|operation|uri        |service               |bodyFilePath|code| temp |
	|GET      |baseWeather|/data/{id}/weather    |            |200 | main.temp |
	
	@weather_pune
	Scenario Outline: Compare the temperature differences of UI and API and validate for a vaild city
    Given user land on the page URL '<url>'
    When select the weather tab on home page
    And select the city '<city>' to get weather details
    Then verify that selected city '<city>' is available on map 
    Then verify that weather info of the city '<city>' is available on map
    Given user wants to perform operation for URI "<uri>"
		When user set the header as below
		|headerkey   |headervalue     |
		|Content-Type|application/json|
		When user set required parameters as below
		|pathParamName|pathParamValue |queryParamName|queryParamValue| #Need to pass empty data if you don't have required to pass any parameter
		| id     | 2.5    |q            | Pune             |
		|        |        |appid        | 7fe67bf08c80ded756e598d6f8fedaea | 
		When user want to perform "<operation>" for "<service>" with "<bodyFilePath>" below parameters
		| |     # Need pass empty data table if you don't required to pass body table
		
		Then webservice should respond responce code "<code>"
		Then get the required value "<temp>" from response
		Then validate the temp differences of UI and API
	
    Examples: 
      | url      | city  |operation|uri        |service               |bodyFilePath|code| temp |
      | NDTV_url | Pune  |GET      |baseWeather|/data/{id}/weather    |            |200 | main.temp | 
